# Annotations

This repository contains manually created annotations that were created with [CVAT](https://github.com/opencv/cvat/) and can be used for research purposes.

## Semantic Segmentation
- coarse annotations for free space detection applied to a subset of sequence Drive A of [LMS fisheye data set](https://fisheyedataset.lms.tf.fau.de/)
